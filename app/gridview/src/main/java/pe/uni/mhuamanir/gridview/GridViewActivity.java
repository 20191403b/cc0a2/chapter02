package pe.uni.mhuamanir.gridview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import java.util.ArrayList;

public class GridViewActivity extends AppCompatActivity {

    GridView gridView;
    ArrayList<String> text=new ArrayList<>();
    ArrayList<Integer> image=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid_view);

        gridView=findViewById(R.id.grid_view);
        fillArray();
        GridAdapter gridAdapter=new GridAdapter(this,text,image);
        gridView.setAdapter(gridAdapter);

        gridView.setOnItemClickListener((parent, view, position, id) -> {
            
        });
    }
    private void fillArray(){
        text.add("Zorro");
        text.add("Conejo");

        image.add(R.drawable.zorro);
        image.add(R.drawable.conejo);
    }
}