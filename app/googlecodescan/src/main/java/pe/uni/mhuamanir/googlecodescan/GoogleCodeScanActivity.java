package pe.uni.mhuamanir.googlecodescan;

//import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
//import android.view.TextureView;
//import android.view.View;
import android.widget.Button;
import android.widget.TextView;

//import com.google.android.gms.tasks.OnCanceledListener;
//import com.google.android.gms.tasks.OnFailureListener;
//import com.google.android.gms.tasks.OnSuccessListener;
import com.google.mlkit.common.MlKitException;
import com.google.mlkit.vision.barcode.common.Barcode;
import com.google.mlkit.vision.codescanner.*;

import com.bosphere.filelogger.FL;//añadido para usar

public class GoogleCodeScanActivity extends AppCompatActivity {

    private static final String TAG=GoogleCodeScanActivity.class.getSimpleName();
    TextView textViewScanner;
    Button buttonScanner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FL.i(TAG,"Create");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google_code_scan);
        textViewScanner=findViewById(R.id.text_view_scanner);
        buttonScanner=findViewById(R.id.button_scanner);

        buttonScanner.setOnClickListener(v -> {
            GmsBarcodeScannerOptions options = new GmsBarcodeScannerOptions.Builder()
                    .setBarcodeFormats(Barcode.FORMAT_QR_CODE,Barcode.FORMAT_AZTEC)
                    .build();

            GmsBarcodeScanner scanner =GmsBarcodeScanning.getClient(getApplicationContext(),options);

            scanner.startScan()
                    .addOnSuccessListener(barcode -> {
                        FL.i("addOnSuccessListener");
                         textViewScanner.setText(getSuccessfulMessage(barcode));
                    })
                    .addOnCanceledListener(() -> {
                        FL.i("addOnCanceledListener");
                        textViewScanner.setText(R.string.cancelle_option);})
                    .addOnFailureListener(e -> {
                        FL.i("addOnFailureListener");
                        textViewScanner.setText(getFailureException((MlKitException) e));});

        });
    }

    private String getSuccessfulMessage(Barcode barcode){
        FL.i(TAG,"get Successful Message");
        return String.format (getResources().getString(R.string.barcode_result)
                ,barcode.getRawValue()
                ,barcode.getFormat()
                ,barcode.getValueType());
    }
    private String getFailureException(MlKitException e){
        FL.i(TAG,"get Failure Exception");
        switch (e.getErrorCode()){
            case MlKitException.CODE_SCANNER_CANCELLED:
                 return getString(R.string.error_code_scanner_cancelled);
            case MlKitException.UNKNOWN:
                 return getString(R.string.error_code_unknown);
            default:
                return getString(R.string.error_default);
        }
    }
}