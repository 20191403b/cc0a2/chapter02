package pe.uni.mhuamanir.intent;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class SecontActivity extends AppCompatActivity {

    TextView textViewText,textViewNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_secont);

        textViewText=findViewById(R.id.text_view_text);
        textViewNumber=findViewById(R.id.text_view_number);

        Intent intent2=getIntent();
        String text=intent2.getStringExtra("TEXT");
        int number=intent2.getIntExtra("NUMBER",0);
        textViewText.setText(text);
        textViewNumber.setText(String.valueOf(number));
    }
}