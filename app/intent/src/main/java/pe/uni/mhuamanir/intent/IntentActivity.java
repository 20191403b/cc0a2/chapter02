package pe.uni.mhuamanir.intent;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

public class IntentActivity extends AppCompatActivity {

    EditText editText,editNumber;
    Button button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intent);

        editText=findViewById(R.id.edit_text);
        editNumber=findViewById(R.id.edit_number);
        button=findViewById(R.id.button);

        button.setOnClickListener(v -> {
            String sText=editText.getText().toString();
            String sNumber=editNumber.getText().toString();

            Intent intent=new Intent(IntentActivity.this,SecontActivity.class);
            intent.putExtra("TEXT",sText);
            if(!sNumber.equals("")){
                int number=Integer.parseInt(sNumber);
                intent.putExtra("NUMBER",number);
            }
            startActivity(intent);
            //finish();
        });

    }
}