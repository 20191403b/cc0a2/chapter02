package pe.uni.mhuamanir.checkbox;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.CheckBox;
import android.widget.TextView;

public class Check_boxActivity extends AppCompatActivity {
    CheckBox checkBoxFemale;
    CheckBox checkBoxMale;
    TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_box);

        textView =findViewById(R.id.text);
        checkBoxFemale=findViewById(R.id.check_box_female);
        checkBoxMale=findViewById(R.id.check_box_male);

        checkBoxFemale.setOnClickListener(v -> {

            if(checkBoxFemale.isChecked()){
                 textView.setText(R.string.text_female);
                 checkBoxMale.setChecked(false);
            }else{
                textView.setText(R.string.text_view);
            }
        });
        checkBoxMale.setOnClickListener(v -> {

            if(checkBoxMale.isChecked()){
                checkBoxFemale.setChecked(false);
                textView.setText(R.string.text_male);
            }else{
                textView.setText(R.string.text_view);
            }
        });
    }
}