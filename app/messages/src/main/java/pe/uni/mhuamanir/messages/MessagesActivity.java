package pe.uni.mhuamanir.messages;

import android.os.Bundle;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.Snackbar;

public class MessagesActivity extends AppCompatActivity {

    Button buttonToast,buttonSnackBar,buttonDialog;
    LinearLayout linearLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages);
        buttonToast=findViewById(R.id.button_message_1);
        buttonSnackBar=findViewById(R.id.button_message_2);
        buttonDialog=findViewById(R.id.button_message_3);
        linearLayout=findViewById(androidx.constraintlayout.widget.R.id.layout);

        buttonToast.setOnClickListener(v -> Toast.makeText(getApplicationContext(),R.string.app_name,Toast.LENGTH_LONG).show());
        buttonSnackBar.setOnClickListener(v -> Snackbar.make(linearLayout,R.string.snack_back_text,Snackbar.LENGTH_INDEFINITE).setAction("ok", v1 -> {}).show());
        buttonDialog.setOnClickListener(v -> {
            AlertDialog.Builder alertDialog=new AlertDialog.Builder(this);
            alertDialog
                    .setTitle("Título")
                    .setMessage("Message text")
                    .setNegativeButton("no", (dialog, which) -> dialog.cancel())
                    .setPositiveButton("yes", (dialog, which) -> {
                        //algo
                    }).show();

            alertDialog.create();
        });
    }
}